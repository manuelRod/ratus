<?php
/**
 * Created by JetBrains PhpStorm.
 * User: manu
 * Date: 11/13/13
 * Time: 1:35 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Application;

class Number extends AbstractFilter
{
    /**
     * @param $number
     * @return bool|mixed
     */
    public function callBack($number)
    {
        return ($number >= 4000) ? true : false;
    }

    /**
     * @return array|mixed
     */
    public function filter()
    {
        $numbers = $this->getData();
        return array_filter($numbers, array($this, "callBack"));
    }

}
