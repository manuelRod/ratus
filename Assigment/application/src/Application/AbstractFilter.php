<?php
/**
 * Created by JetBrains PhpStorm.
 * User: manu
 * Date: 11/13/13
 * Time: 1:37 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Application;

abstract class AbstractFilter
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->setData($data);
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Callback to filter the array data set
     *
     * @param $arg
     * @return mixed
     */
    abstract function callBack($arg);

    /**
     * Filter function
     *
     * @return mixed
     */
    abstract function filter();

}
