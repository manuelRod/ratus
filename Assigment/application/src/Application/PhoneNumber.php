<?php
/**
 * Created by JetBrains PhpStorm.
 * User: manu
 * Date: 11/13/13
 * Time: 1:31 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Application;

class PhoneNumber extends AbstractFilter
{
    const FIFTEEN = '15';

    /**
     * Callback function to filter phone numbers ending in 15
     *
     * @param $number
     * @return bool
     */
    public function callBack($number)
    {
        return (preg_match('#.+' . self::FIFTEEN . '#', $number));
    }

    /**
     * @return array|mixed
     */
    public function filter()
    {
        $phoneNumbers = $this->getData();
        return array_filter($phoneNumbers, array($this, "callBack"));
    }

}
