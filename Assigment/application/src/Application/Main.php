<?php

namespace Application;

class Main
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->setData($data);
    }

    /**
     * @param array $data
     */
    public function setData(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    protected function getPeople()
    {
        return $this->data['people'];
    }

    /**
     * @return mixed
     */
    protected function getCars()
    {
        return $this->data['cars'];
    }

    /**
     * @return mixed
     */
    protected function getPhoneNumbers()
    {
        return $this->data['phoneNumbers'];
    }

    /**
     * @return mixed
     */
    protected function getNumbers()
    {
        return $this->data['numbers'];
    }

    /**
     * Return array List with Doe as a LastName
     *
     * @return array
     */
    public function getLastNameDoeList()
    {
        $doe = new Doe($this->getPeople());
        return $doe->filter();
    }

    /**
     * Return array List with 5 as a number in the model
     *
     * @return array
     */
    public function getCarWithFiveList()
    {
       $car = new Car($this->getCars());
       return $car->filter();
    }

    /**
     * @return array|mixed
     */
    public function getPhoneNumbersEndingFifteen()
    {
        $phoneNumbers = new PhoneNumber($this->getPhoneNumbers());
        return $phoneNumbers->filter();
    }

    /**
     * @return array|mixed
     */
    public function getNumbersGreaterOrEqual4000()
    {
        $numbers = new Number($this->getNumbers());
        return $numbers->filter();
    }

}
