<?php
/**
 * Created by JetBrains PhpStorm.
 * User: manu
 * Date: 11/13/13
 * Time: 1:22 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Application;

class Doe extends AbstractFilter
{
    const DOE = 'Doe';

    /**
     * Callback function to filter if the person is Doe or not
     *
     * @param $person
     * @return bool
     */
    public function callBack($person)
    {
        return ($person['lastName'] == self::DOE) ? true : false;
    }

    /**
     * Return array List with Doe as a LastName
     *
     * @return array
     */
    public function filter()
    {
        return array_filter($this->getData(), array($this, "callBack"));
    }

}
