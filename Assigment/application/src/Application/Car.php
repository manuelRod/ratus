<?php
/**
 * Created by JetBrains PhpStorm.
 * User: manu
 * Date: 11/13/13
 * Time: 1:29 PM
 * To change this template use File | Settings | File Templates.
 */

namespace Application;

class Car extends AbstractFilter
{
    const FIVE = '5';

    /**
     * Callback function to filter 5 in the model name
     *
     * @param $model
     * @return bool
     */
    public function callBack($model)
    {
        return (is_int(strpos($model, self::FIVE))) ? true : false;
    }

    /**
     * Return array List with 5 as a number in the model
     *
     * @return array
     */
    public function filter()
    {
        $cars = $this->getData();

        foreach ($cars['brands'] as $brandName => $brand)
        {
            $filterModels = array_filter($brand['models'], array($this, "callBack"));
            $cars['brands'][$brandName]['models']  = $filterModels;
        }

        return $cars;
    }

}
